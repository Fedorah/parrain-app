from User.user import User
import json


class Profile:
    def __init__(self, questionnaire, description, photo_url):
        self.questionnaire = questionnaire
        self.description = description
        self.photo_url = photo_url

    def calculate_all_score(self, list_of_users, current_user_id):
        score_list = []
        for user in list_of_users:
            if user.id == current_user_id:
                continue
            score_list.append(self.two_profiles_score(user))
        return score_list

    def two_profiles_score(self, second_user):
        score = []
        for question, response in self.questionnaire.items():
            if "coef" in response:
                for modalities, value in response.items():
                    if (
                        (
                            second_user.profile.questionnaire[question][modalities]
                            and value
                        ) is True
                    ):  
                        score.append(response["coef"])
        return sum(score)

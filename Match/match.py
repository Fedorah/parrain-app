from User.user import User
from Session.session import Session


class Match:
    def __init__(self, id, id1, id2):
        self.id = id
        self.id1 = id1
        self.id2 = id2

    def make_match(self):
        payload = {
            "request_type": "like",
            "liked": None,
        }
        if self.session.session_data["like"]:
            self.session.user.like_users.append(self.user_to_match)
            # update db
            payload["liked"] = True
        else:
            # update db ?
            payload["liked"] = False
        self.session.update_client(
            "127.0.0.1:8000/post", payload
        )  # send request to server to update client

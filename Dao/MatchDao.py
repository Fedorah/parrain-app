import psycopg2
from Match.match import Match

class MatchDao:

    def get_liste_match_by_user(conn, user):
        """
            Classe Match(id_user_1, id_user_2, state)
            state vaut 0 ou 1, 0 = refus, 1 = like
        """
        res = []
        cur = conn.cursor()
        try:
            user_id = user.id
            cur.execute("SELECT * FROM Match WHERE user1={} AND like_dislike='Like';".format(user_id))
            listeTuples = cur.fetchall()
            for c in listeTuples:
               match = Match(c[0], c[1], c[2])
               res.append(match)
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()
            return res


    def create_match(conn, user1, user2, state):
        cur = conn.cursor()
        match = None
        try:
            cur.execute("INSERT INTO Match (user1, user2, like_dislike) VALUES ({}, {}, '{}');".format(user1.id, user2.id, state))
            conn.commit()
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()

    def is_reciproque(conn, user1, user2):
        cur = conn.cursor()
        res = False
        try:
            cur.execute("SELECT * FROM Match WHERE user1={} AND user2={} AND like_dislike='{}';".format(user2.id, user1.id, "Like"))
            c = cur.fetchone()
            if len(c) > 0:
               res = True
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()
            return res

    def is_match(conn, user1, user2):
        cur = conn.cursor()
        res = False
        try:
            cur.execute("SELECT * FROM Match WHERE user1={} AND user2={};".format(user1.id, user2.id))
            c = cur.fetchone()
            if len(c) > 0:
                res = True
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()
            return res
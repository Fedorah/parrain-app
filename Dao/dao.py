import psycopg2


class Dao:

    def connection(host, dbName, user, password):
        conn = psycopg2.connect(host=host, dbname=dbName, user=user, password=password)
        return conn

    def create_table(table_name: str, columns: dict):
        # NOTE make it simpler like the other methods !
        """
        columns = [
            {
                "col_name": "",
                "data_type": "",
                "PRIMARY KEY": false,
                "FOREING KEY": false,
                "NOT NULL": false,
            },
        ]
        """
        request = "CREATE TABLE [IF NOT EXIST]"
        request += table_name + "( \n"
        for column in columns:
            for key, val in column:
                if val is str:
                    request += key + " "
                elif val:
                    request += key + " "
        request += ";"
        return request

    @staticmethod
    def insert_into(columns, values, table_name):
        """
        columns = "(column1,column2 ,..)"
        values = "(value1, value2, ...)"
        """
        request = "INSERT INTO " + table_name + " " + columns + " VALUES" + values + ";"
        return request

    @staticmethod
    def read_table(columns, table, condition):
        request = "SELECT "
        for column in columns:
            if not column == columns[-1]:
                request += column + ", "
            else:
                request += column + " "
        request += "FROM " + table + " "
        if condition is not None:
            request += "WHERE " + condition
        return request + ";"

    @staticmethod
    def update_table(table_name, columns_and_values, condition):
        """
        columns_and_values = {
            "column_1": value_1,
            "column_2": value_2,
            ...
        }
        """
        request = "UPDATE " + table_name + "\nSET "
        count = 0
        for key, val in columns_and_values:
            if count != len(list(columns_and_values.keys())):
                request += key + " = " + val + ",\n"
            else:
                request += key + " = " + val + "\n"
            count += 1
        request += "WHERE " + condition
        return request

    @staticmethod
    def delete_table(table_name, condition):
        request = "DELETE FROM " + table_name + " WHERE " + condition + ";"
        return request

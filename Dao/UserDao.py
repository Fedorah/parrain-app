import psycopg2
from User.user import User

class UserDao:

    def getListeUsers(conn):
        res = []
        cur = conn.cursor()
        try:
            cur.execute("SELECT * FROM Utilisateur;")
            listeTuples = cur.fetchall()
            for c in listeTuples:
                user = User(c[0], c[1], c[2], c[3], c[4])
                res.append(user)
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()
            return res

    def test_connexion(conn, nom, hash):
        user = None
        cur = conn.cursor()
        try:
            cur.execute("SELECT * FROM utilisateur WHERE nom='{}' AND hash_mdp='{}';".format(nom, hash))
            c = cur.fetchone()
            if len(c) > 0:
                user = User(c[0], c[1], c[2], c[3], c[4])
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()
            return user

    def get_user_by_id(conn, id):
        user = None
        cur = conn.cursor()
        try:
            cur.execute("SELECT * FROM utilisateur WHERE idutilisateur={};".format(id))
            c = cur.fetchone()
            if len(c) > 0:
                user = User(c[0], c[1], c[2], c[3], c[4])
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()
            return user
    

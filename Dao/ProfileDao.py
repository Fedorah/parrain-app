import psycopg2
from Profile.profile import Profile
import Tools.Generer_json

class ProfileDao:

    def get_profile_by_user(conn, user):
        profile = None
        cur = conn.cursor()
        try:
            user_id = user.id
            cur.execute("SELECT * FROM profile WHERE iduser={};".format(user_id))
            c = cur.fetchone()
            if len(c) > 0:
                questionnaire = Tools.Generer_json.questionnaire_to_json(c[3], c[4], c[5], c[6], c[7], c[8])
                profile = Profile(questionnaire, c[1], c[2])
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()
            return profile

    
    

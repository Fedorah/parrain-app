import psycopg2

class ChoixDao:

    def create_choice(conn, id1, id2):
            cur = conn.cursor()
            try:
                cur.execute("INSERT INTO Choix (user1, user2) VALUES ({}, {});".format(id1, id2))
                conn.commit()
            except psycopg2.Error as error:
                raise error
            finally:
                cur.close()

    def choix_fait(conn, user):
        cur = conn.cursor()
        res = False
        try:
            cur.execute("SELECT * FROM Choix WHERE user1 = {};".format(user.id))
            c = cur.fetchone()
            if len(c) > 0:
               res = True
        except psycopg2.Error as error:
            raise error
        finally:
            cur.close()
            return res


class User:

    def __init__(self, id, nom, prenom, status, telephone=None, profile=None):
        self.id = id
        self.nom = nom
        self.prenom = prenom
        self.status = status
        self.telephone = telephone
        self.profile = profile
        self.ranking = None
        self.liked_users = []

    def make_profile_ranking(self, user_list):
        self.ranking = sorted(self.profile.calculate_all_score(user_list, self.id), reverse=True)

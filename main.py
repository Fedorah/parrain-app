from User.user import User
from Profile.profile import Profile
import json
from fastapi import FastAPI
from db_properties import host, database, user, password
from Dao.dao import Dao
import uvicorn
from Service.UserService import UserService
from fastapi.middleware.cors import CORSMiddleware
import Tools.Hash_mdp


app = FastAPI()

origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)




conn = Dao.connection(host, database, user, password)
dict_keys = {}


@app.get("/")
def root():
    return {"result": "ok"}

@app.get("/Connexion/{nom}/{mdp}")
def getConnexion(nom, mdp):
    user_dict = UserService.testConnection(nom, mdp, conn)
    if user_dict["Status"] == "OK":
        key = user_dict["Id"]
        id = user_dict["Id"]
        dict_keys[key] = id
    return UserService.testConnection(nom, mdp, conn)

@app.get("/ConnexionChoix/{nom}/{mdp}")
def getConnexionChoix(nom, mdp):
    user_dict = UserService.testConnectionChoix(nom, mdp, conn)
    return user_dict

@app.get("/Choix/{id}/{id1}/{id2}/{id3}")
def getChoice(id, id1, id2, id3):
    return UserService.enregistrerChoix(id, id1, id2, id3, conn)

@app.get("/Like/{key}/{id1}/{id2}")
def getLike(key, id1, id2):
    res = UserService.makeLike(id1, id2, conn)
    res["Key"] = key
    return res

@app.get("/Dislike/{key}/{id1}/{id2}")
def getDislike(key, id1, id2):
    res = UserService.makeDislike(id1, id2, conn)
    res["Key"] = key
    return res

@app.get("/Matchs/{id}/")
def getMatchs(id):
    id_user = id
    matchs_json = UserService.getMatchList(id_user, conn)
    return {
        "Titre" : "Matchs",
        "Key" : id,
        "Liste_matchs" : matchs_json
    }


if __name__ == "__main__":
    uvicorn.run(app, host="151.106.109.133", port=5000)

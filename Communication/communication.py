from typing import Tuple
import requests


class Communication:

    def __init__(self, host: Tuple, domain: str = None):
        """
            host = (ip_address, port)
        """
        self.host = host
        self.url = self.host[0] + ":" + host[1] + "/"
    
    def get(self, params=""):
        """
            If params are to be passed, it must be a dict.
        """
        r = requests.get(url=self.url, params=params)
        if r.status_code == 200:
            data = r.json()
        else:
            data = "Error"
        return data
    
    def post(self, params=""):
        """
            If params are to be passed, it must be a dict.
        """
        r = requests.post(url=self.url, params=params)
        if r.status_code == 200:
            data = r.json()
        else:
            data = "Error"
        return data

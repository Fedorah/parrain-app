DROP TABLE IF EXISTS Utilisateur CASCADE;
DROP TABLE IF EXISTS Profile CASCADE;
DROP TABLE IF EXISTS Match CASCADE;
DROP SEQUENCE IF EXISTS idUtilisateur_seq;
DROP SEQUENCE IF EXISTS idProfile_seq;
DROP SEQUENCE IF EXISTS idMatch_seq;

CREATE SEQUENCE idUtilisateur_seq;
CREATE TABLE Utilisateur
(
    idUtilisateur integer NOT NULL DEFAULT nextval
    ('idUtilisateur_seq'::regclass) PRIMARY KEY UNIQUE,
    nom text,
    prenom text,
    promo text,
    telephone text,
    hash_mdp text
);

CREATE SEQUENCE idProfile_seq;
CREATE TABLE Profile
(
    idProfile integer NOT NULL DEFAULT nextval
    ('idProfile_seq'::regclass) PRIMARY KEY UNIQUE,
    description_user text,
    photo_url text,
    q1 text,
    q2 text,
    q3 text,
    q4 text,
    q5 text,
    q6 text,
    idUser integer REFERENCES Utilisateur ON DELETE CASCADE
);


CREATE SEQUENCE idMatch_seq;
CREATE TABLE Match
(
    idMatch integer NOT NULL DEFAULT nextval
	('idMatch_seq'::regclass) PRIMARY KEY UNIQUE,
	user1 integer REFERENCES Utilisateur ON DELETE CASCADE,
    user2 integer REFERENCES Utilisateur ON DELETE CASCADE,
    like_dislike text
);

CREATE SEQUENCE idChoix_seq
CREATE TABLE Choix
(
    idChoix integer NOT NULL DEFAULT nextval
	('idChoix_seq'::regclass) PRIMARY KEY UNIQUE,
    user1 integer REFERENCES Utilisateur ON DELETE CASCADE,
	user2 integer REFERENCES Utilisateur ON DELETE CASCADE
);
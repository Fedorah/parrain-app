import hashlib

def hash_mdp(mdp):
    return hashlib.sha224(bytes(mdp, 'utf-8')).hexdigest()
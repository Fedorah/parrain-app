import random
import time

def generer_key():
    random.seed(time.time)
    key = ""
    for i in range(10):
        key += str(random.randint(0,9))
    return key


def questionnaire_to_json(q1, q2, q3, q4, q5, q6):
    res = {}

    
    if q1 == "mod1":
        res["Tu viens d'une filière éco ou MP ?"] = "Eco"
    else:
        res["Tu viens d'une filière éco ou MP ?"] = "MP"
        
    if q2 == "mod1":
        res["Quel genre de moments souhaites tu partager avec ton parrain ou ton filleul ?"] = "Chill (resto, cinéma...)"
    elif q2 == "mod2":
        res["Quel genre de moments souhaites tu partager avec ton parrain ou ton filleul ?"] = "Enflammée (bar, musique, beaucoup de monde...)"
    else:
        res["Quel genre de moments souhaites tu partager avec ton parrain ou ton filleul ?"] = "Travail, aide pour les cours"

    if q3 == "mod1":
        res["Tu es plutôt"] = "Lune de miel (voyage, sport, avanture...)"
    elif q3 == "mod2":
        res["Tu es plutôt"] = "Netflix and chill (lecture, cinéma, musique...)"
    else:
        res["Tu es plutôt"] = "Repas aux chandelles (ptit resto, alcool)"

    if q4 == "mod1":
        res["En musique, tu es plutôt"] = "Team Rap"
    elif q4 == "mod2":
        res["En musique, tu es plutôt"] = "Team techno"
    else:
        res["En musique, tu es plutôt"] = "Autre"
    
    if q5 == "mod1":
        res["Ton Star Wars préféré ?"] = "L'Empire contre-attaque"
    elif q5 == "mod2":
        res["Ton Star Wars préféré ?"] = "Le retour du Jedi"
    else:
        res["Ton Star Wars préféré ?"] = "La revanche des Sith"
    
    res["Quels sont les cinq traits de caractère qui te définissent le plus ? "] = q6

    return res

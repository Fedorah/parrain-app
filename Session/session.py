from User.user import User
from Dao.dao import Dao
from Dao.UserDao import UserDao
import requests
import hashlib


class Session:

    def __init__(self, logs_info: dict, connection_status: bool):
        self.logs_info = logs_info
        self.connection_status = connection_status
        self.user = None

    def make_user(self):
        user = UserDao.get_user_by_id(self.conn, self.logs_info["user_id"]) # dummy name, need to be replaced when implemented
        self.user = user
        return self.user

    def validate_login(self):
        user_id = self.logs_info["user_id"]
        hash = hashlib.sha256(self.logs_info["password"])
        test_connexion = Dao().test_connexion(self.conn, user_id, hash)
        if test_connexion is not None:
            self.session_connection_status = True
        else:
            self.session_connection_status = False
        return self.session_connection_status

    def update_client(self, request, payload):
        """
            request: str, the http request to send
            payload: dict, parameters of the request
        """
        requests.post(request, data=payload)

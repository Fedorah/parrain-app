from pprint import pprint
import json
import random

with open("questionnaire.json", "r", encoding="utf-8") as json_file:
    questionnaire = json.load(json_file)


def generate():
    # NOTE Not working, make it work !
    new_form = {}
    for question, response in questionnaire.items():
        new_form[question] = response
        if "coef" in response:
            if random.random() > 0.5:
                pass
    return new_form


if __name__ == "__main__":
    pprint(generate())

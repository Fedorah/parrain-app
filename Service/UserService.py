from Dao.ChoixDao import ChoixDao
from Match.match import Match
from User.user import User
from Dao.UserDao import UserDao
from Dao.ProfileDao import ProfileDao
from Dao.MatchDao import MatchDao
import Tools.Generer_key
import Tools.Hash_mdp

class UserService:

    def testConnectionChoix(nom, mdp, conn):
        user = UserDao.test_connexion(conn, nom, Tools.Hash_mdp.hash_mdp(mdp))
        liste_matchs = []
        if user != None:
            if ChoixDao.choix_fait(conn, user):
                return {"Titre" : "Connexion", "Status" : "ALREADY"}
            matchs = MatchDao.get_liste_match_by_user(conn, user)
            for match in matchs:
                user2 = UserDao.get_user_by_id(conn, match.id2)
                if MatchDao.is_reciproque(conn, user, user2):
                    profile2 = ProfileDao.get_profile_by_user(conn, user2)
                    liste_matchs.append({"Nom" : user2.nom, "Prénom" : user2.prenom, "Id" : user2.id, "Téléphone" : user2.telephone, "Description" : profile2.description, "Photo_url" : profile2.photo_url})

            return {"Titre" : "Connexion", "Status" : "OK", "Id" : user.id, "Liste_Matchs" : liste_matchs}
        else:
            return {"Titre": "Connexion", "Status": "NOT OK"}


    def testConnection(nom, mdp, conn):
        user = UserDao.test_connexion(conn, nom, Tools.Hash_mdp.hash_mdp(mdp))
        if user != None:
            profile = ProfileDao.get_profile_by_user(conn, user)
            print(profile.questionnaire)
            res = {"Titre": "Connexion",
                   "Status": "OK",
                   "Nom": user.nom,
                   "Prénom": user.prenom,
                   "Id": user.id,
                   "Promo": user.status,
                   "Téléphone": user.telephone,
                   "Ranking": {},
                   "Profile": {
                       "Description" : profile.description,
                       "Photo_url" : profile.photo_url,
                       "Questionnaire" : profile.questionnaire
                    },
                   "Key" : user.id
                   }
            users = UserDao.getListeUsers(conn)
            ranking = []
            for i in range(len(users)):
                u = users[i]
                if u.id != user.id and u.status != user.status and not(MatchDao.is_match(conn, user, u)):
                    p = ProfileDao.get_profile_by_user(conn, u)
                    ranking.append( {
                        "Id" : u.id,
                        "Nom" : u.nom,
                        "Prénom" : u.prenom,
                        "Photo_url" : p.photo_url,
                        "Description" : p.description,
                        "Questionnaire" : p.questionnaire
                    } )
            res["Ranking"] = ranking

            return res
        else:
            return {"Titre": "Connexion", "Status": "NOT OK"}

    def enregistrerChoix(id, id1, id2, id3, conn):
        user = UserDao.get_user_by_id(conn, id)
        if not(ChoixDao.choix_fait(conn, user)):
            ChoixDao.create_choice(conn, id, id1)
            ChoixDao.create_choice(conn, id, id2)
            ChoixDao.create_choice(conn, id, id3)
            return {"Status" : "OK"}
        else:
            return {"Status" : "NOT OK"}


    def makeDislike(id1, id2, conn):
        user1 = UserDao.get_user_by_id(conn, id1)
        user2 = UserDao.get_user_by_id(conn, id2)
        if not(MatchDao.is_match(conn, user1, user2)):
            MatchDao.create_match(conn, user1, user2, "Dislike")
        res = {
            "Titre" : "Dislike",
            "Status" : "OK"
        }
        return res

    def makeLike(id1, id2, conn):
        user1 = UserDao.get_user_by_id(conn, id1)
        user2 = UserDao.get_user_by_id(conn, id2)
        if not(MatchDao.is_match(conn, user1, user2)):
            MatchDao.create_match(conn, user1, user2, "Like")
        res = {
            "Titre" : "Like",
            "Key" : "testkey",
            "User1" : user1.id,
            "User2" : user2.id,
            "Match" : MatchDao.is_reciproque(conn, user1, user2)
        }
        return res

    def getMatchList(id_user, conn):
        user = UserDao.get_user_by_id(conn, id_user)
        matchs = MatchDao.get_liste_match_by_user(conn, user)
        matchs_json = []
        for match in matchs:
            user2 = UserDao.get_user_by_id(conn, match.id2)
            if MatchDao.is_reciproque(conn, user, user2):
                profile2 = ProfileDao.get_profile_by_user(conn, user2)
                matchs_json.append({
                    "Id" : user2.id,
                    "Nom" : user2.nom,
                    "Prénom" : user2.prenom,
                    "Photo_url" : profile2.photo_url,
                    "Description" : profile2.description,
                    "Téléphone" : user2.telephone,
                    "Questionnaire" : profile2.questionnaire
                })
        return matchs_json